require 'spec_helper'

describe 'gitlab_omnibus::default' do
  # Simply using centos as a test because we have to have some platform.
  # Truly platform specific things will be tested separately, if necessary.
  context 'with default attributes' do
    context 'on first install' do
      before do
        allow(File).to receive(:exist?).and_call_original
        allow(File).to receive(:exist?).with('/opt/gitlab/bin/gitlab-ctl').and_return(false)
      end
      cached(:chef_run) do
        ChefSpec::SoloRunner.new(
          platform: 'centos', version: '6.5', file_cache_path: '/var/tmp'
        ).converge(described_recipe)
      end
      subject { chef_run }

      it { is_expected.to install_package('gitlab-ce').with_version(nil) }
      it { is_expected.to include_recipe('gitlab_omnibus::_backup') }
      it { is_expected.not_to include_recipe('gitlab_omnibus::_ci_backup') }

      it do
        expect(
          chef_run.template('/etc/gitlab/gitlab.rb')
        ).to notify('gitlab_omnibus_service[gitlab]').to(:reconfigure).immediately
      end

      it do
        expect(
          chef_run.template('/etc/gitlab/gitlab.rb')
        ).not_to notify('gitlab_omnibus_service[gitlab]').to(:restart).delayed
      end

      it do
        expect(
          chef_run.package('gitlab-ce')
        ).not_to notify('gitlab_omnibus_service[gitlab]').to(:reconfigure).delayed
      end

      it do
        expect(
          chef_run.package('gitlab-ce')
        ).not_to notify('gitlab_omnibus_service[gitlab]').to(:restart).delayed
      end

      it do
        is_expected.to render_file('/etc/gitlab/gitlab.rb')
          .with_content(<<EOS
# This file is managed by Chef, using the gitlab_omnibus cookbook.
# Editing this file by hand is highly discouraged!

external_url 'https://fauxhai.local'
EOS
          )
      end
    end

    context 'on subsequent runs' do
      before do
        allow(File).to receive(:exist?).and_call_original
        allow(File).to receive(:exist?).with('/opt/gitlab/bin/gitlab-ctl').and_return(true)
      end
      cached(:chef_run) do
        ChefSpec::SoloRunner.new(
          platform: 'centos', version: '6.5', file_cache_path: '/var/tmp'
        ).converge(described_recipe)
      end
      subject { chef_run }

      it do
        expect(
          chef_run.template('/etc/gitlab/gitlab.rb')
        ).to notify('gitlab_omnibus_service[gitlab]').to(:reconfigure).delayed
      end

      it do
        expect(
          chef_run.template('/etc/gitlab/gitlab.rb')
        ).to notify('gitlab_omnibus_service[gitlab]').to(:restart).delayed
      end

      it do
        expect(
          chef_run.package('gitlab-ce')
        ).to notify('gitlab_omnibus_service[gitlab]').to(:reconfigure).delayed
      end

      it do
        expect(
          chef_run.package('gitlab-ce')
        ).to notify('gitlab_omnibus_service[gitlab]').to(:restart).delayed
      end
    end
  end

  context 'with non-default attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(platform: 'centos',
                               version: '6.5',
                               file_cache_path: '/var/tmp') do |node|
        node.set['gitlab_omnibus']['gitlab_rails']['omniauth_enabled'] = true
        node.set['gitlab_omnibus']['gitlab_rails']['omniauth_providers'] =
          [
            {
              'name' => 'google_oauth2',
              'app_id' => 'YOUR APP ID',
              'app_secret' => 'YOUR APP SECRET',
              'args' => { 'access_type' => 'offline', 'approval_prompt' => '' }
            }
          ]
        node.set['gitlab_omnibus']['gitlab_rails']['gitlab_restricted_visibility_levels'] = nil
        node.set['gitlab_omnibus']['gitlab_rails']['ldap_servers'] =
          { 'main' => { 'label' => 'LDAP', 'host' => '_your_ldap_server' } }
        node.set['gitlab_omnibus']['high_availability']['mountpoint'] = '/var/opt/gitlab'
        node.set['gitlab_omnibus']['backup']['enable'] = false
        node.set['gitlab_omnibus']['git_data_dir'] = '/home/gitlab/git-data'
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'git_data_dir \'/home/gitlab/git-data\'',
      'gitlab_rails[\'omniauth_enabled\'] = true',
      '[{"name"=>"google_oauth2", "app_id"=>"YOUR APP ID", ' \
        '"app_secret"=>"YOUR APP SECRET", "args"=>{' \
        '"access_type"=>"offline", "approval_prompt"=>""}}]',
      'gitlab_rails[\'gitlab_restricted_visibility_levels\'] = nil',
      '{"main"=>{"label"=>"LDAP", "host"=>"_your_ldap_server"}}',
      'high_availability[\'mountpoint\'] = \'/var/opt/gitlab\''
    ].each do |content|
      it do
        is_expected.to render_file('/etc/gitlab/gitlab.rb')
          .with_content(content)
      end
    end

    it { is_expected.not_to include_recipe('gitlab_omnibus::_backup') }
  end

  context 'when installing from an internal repository' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(platform: 'centos',
                               version: '6.5',
                               file_cache_path: '/var/tmp') do |node|
        node.set['gitlab_omnibus']['package_name'] = 'my-gitlab'
      end.converge(described_recipe)
    end
    subject { chef_run }

    it { is_expected.not_to create_packagecloud_repo('gitlab/gitlab-ce') }
    it { is_expected.to install_package('my-gitlab') }
  end

  context 'when installing gitlab enterprise' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(platform: 'centos',
                               version: '6.5',
                               file_cache_path: '/var/tmp') do |node|
        node.set['gitlab_omnibus']['edition'] = 'enterprise'
      end.converge(described_recipe)
    end
    subject { chef_run }

    it { is_expected.to create_packagecloud_repo('gitlab/gitlab-ee') }
    it { is_expected.to install_package('gitlab-ee') }
  end
end
