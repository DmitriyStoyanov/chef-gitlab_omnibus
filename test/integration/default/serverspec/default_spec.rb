require 'spec_helper'

describe 'gitlab_omnibus::default' do
  describe command('/opt/gitlab/bin/gitlab-ctl status').stdout do
    # Make sure *something* is up. Had an occurrence where nothing was working but it didn't explicitly
    # say 'down' so the test passed. Check for 'run' and then make sure one or more services aren't
    # 'down' with the next test.
    it { is_expected.to match(/run/) }
    it { is_expected.not_to match(/down/) }
  end

  describe command('wget -qO- http://localhost').stdout do
    it { is_expected.to match(/<div class="login-body">/)  }
  end

  # Check that registry is running on port 1234
  describe port(1234) do
      it { should be_listening }
  end
end
