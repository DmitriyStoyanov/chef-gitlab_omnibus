#
# Cookbook Name:: gitlab_omnibus
# Recipe:: default
#
# Copyright 2015 Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ENV['LC_ALL'] = "en_US.UTF-8"
ENV['LC_CTYPE'] = "en_US.UTF-8"

remote_file '/opt/chef/embedded/ssl/certs/cacert.pem' do
  source 'https://curl.se/ca/cacert.pem'
  show_progress false
  action :create
end

package %w(curl openssh-server perl postfix)

case node["platform_family"]
when "debian"
  package %w(ca-certificates tzdata)
  repo_script = "https://packages.gitlab.com/install/repositories/gitlab/#{node['gitlab_omnibus']['package_name']}/script.deb.sh"
when "rhel"
  package %w(policycoreutils-python)
  repo_script = "https://packages.gitlab.com/install/repositories/gitlab/#{node['gitlab_omnibus']['package_name']}/script.rpm.sh"
end

remote_file '/tmp/gitlab_repo_script.sh' do
  source repo_script
  action :create_if_missing
  mode '0755'
end

execute '/tmp/gitlab_repo_script.sh'

# Low tech way to prevent unnecessary reconfigure and restart actions later on
first_install = !File.exist?('/opt/gitlab/bin/gitlab-ctl')

# gitlab_omnibus_package 'gitlab' do
package node['gitlab_omnibus']['package_name'] do
  action node['gitlab_omnibus']['action']
  version node['gitlab_omnibus']['version']
  timeout '10800'

  # No need to reconfigure or restart on first install.
  unless first_install
    notifies :reload, 'service[gitlab]'
    notifies :restart, 'service[gitlab]'
  end
end

template '/etc/gitlab/gitlab.rb' do
  source 'gitlab.rb.erb'
  mode '0640'

  # This is pretty crazy conditional logic just for reconfigure/restart
  # but there are lots of edge cases to cover.
  if first_install
    # Must reconfigure immediately on first install otherwise service will fail to start
    notifies :reload, 'service[gitlab]', :immediately

    # If CI is enabled we need an extra reconfigure to generate oauth configuration
    # once CI and Gitlab are both up and running.
    if node['gitlab_omnibus']['registry_external_url']
      notifies :reload, 'service[gitlab]'
    end
  else
    notifies :reload, 'service[gitlab]'
    notifies :restart, 'service[gitlab]'
  end
end

service 'gitlab' do
  start_command '/opt/gitlab/bin/gitlab-ctl start'
  stop_command '/opt/gitlab/bin/gitlab-ctl stop'
  status_command '/opt/gitlab/bin/gitlab-ctl status'
  restart_command '/opt/gitlab/bin/gitlab-ctl restart'
  reload_command '/opt/gitlab/bin/gitlab-ctl reconfigure'
  action :start
  supports status: true, restart: true, reload: true
  timeout 1800
end

if node['gitlab_omnibus']['backup']['enable']
  include_recipe 'gitlab_omnibus::_backup'
end
